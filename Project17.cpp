﻿
#include <iostream>
#include <cmath>

class Vector
{
private:

    double x;
    double y;
    double z;

public:

    Vector() : x(3), y(4), z(-5)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z << std::endl;
    }
    double LongVector()
    {
        double sum;

        sum = abs( sqrt (pow(x, 2) + pow(y, 2) + pow(z, 2)));
        std::cout << sum << std::endl;
        return sum;

    }
};

int main()
{
    Vector V;
    V.Show();
    V.LongVector();
    Vector V1(10,12,-8);
    V1.Show();
    V1.LongVector();
}
